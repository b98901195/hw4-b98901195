package ntu.csie.oop13spring;
import ntu.csie.oop13spring.*;

class POOBaseAttack extends POOSkill{
	public void act(POOPet pet){
		int hp = pet.getHP();
		if( hp >= 5 )
			pet.setHP(hp-5);
		else
			pet.setHP(0);
	}
}

class POOExecute extends POOSkill{
	public void act(POOPet pet){
		int hp = pet.getHP();
		if( hp > 20 )
			pet.setHP(hp-3);
		else
			pet.setHP(0);
	}
}

class POOBrutalStrike extends POOSkill{
	public void act(POOPet pet){
		int hp = pet.getHP();
		if( hp >= 10 )
			pet.setHP(hp-10);
		else
			pet.setHP(0);
	}
}

class POOPoisonBite extends POOSkill{
	public void act(POOPet pet){
		int hp = pet.getHP();
		if( hp > 0 )
			pet.setHP((int)Math.ceil(hp*0.5));
	}
}

class POOThunderAttack extends POOSkill{
	public void act(POOPet pet){
		int hp = pet.getHP();
		if( hp >= 15 )
			pet.setHP(hp-15);
		else
			pet.setHP(0);
	}
}

class POOChargeElectricity extends POOSkill{
	public void act(POOPet pet){
		int mp = pet.getMP();
		if( mp+15 < 1024 )
			pet.setMP(mp+15);
		else
			pet.setMP(1023);
	}
}

class POORestoreMana extends POOSkill{
	public void act(POOPet pet){
		int mp = pet.getMP();
		if( mp+10 < 1024 )
			pet.setMP(mp+10);
		else
			pet.setMP(1023);
	}
}

class POOSlowAttack extends POOSkill{
	public void act(POOPet pet){
		int agi = pet.getAGI();
		int hp = pet.getHP();
		pet.setAGI(1);
		if( hp > 0 )
			pet.setHP(hp-1);
	}
}

class POOBloodThirst extends POOSkill{
	public void act(POOPet pet){
		int hp = pet.getHP();
		int mp = pet.getMP();
		if( hp >= 5 )
			pet.setHP(hp-5);
		else
			pet.setHP(0);
		if( mp >= 5 )
			pet.setMP(mp-5);
		else
			pet.setMP(0);
	}
}

class POOBloodConvert extends POOSkill{
	public void act(POOPet pet){
		int hp = pet.getHP();
		int mp = pet.getMP();
		if( hp > 5 ){
			pet.setHP(hp-5);
			if( mp+10 < 1024 )
				pet.setMP(mp+10);
			else
				pet.setMP(1023);
		}
	}
}

class POOHeal extends POOSkill{
	public void act(POOPet pet){
		int hp = pet.getHP();
		int mp = pet.getMP();
		if( hp+30 < 1024 )
			pet.setHP(hp+30);
		else
			pet.setHP(1023);
		if( mp >= 20 )
			pet.setMP(mp-20);
		else
			pet.setMP(0);
	}
}

class POOCrossAttack extends POOSkill{
	public void act(POOPet pet){
		int hp = pet.getHP();
		if( hp >= 10 )
			pet.setHP(hp-10);
		else
			pet.setHP(0);
	}
}
