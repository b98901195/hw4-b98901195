package ntu.csie.oop13spring;
import ntu.csie.oop13spring.*;

class POOLion extends POONewPet{
	POOLion(){
		this.setHP(75);
		this.setMP(50);
		this.setAGI(5);
		this.setName("Lion");
		this.setCoordinate(15,15);
	}

	protected POOAction act(POOArena arena){
		POOTarget target = find_target(arena);
		if( target.exists == true ){
			if( target.distance < 1 ){
				return this.attack((POONewPet)target.pet);
			}
		}
		return null;
	}

	protected POOCoordinate move(POOArena arena){
		POOTarget target = find_target(arena);
		if( target.exists == true ){
			return this.move((POONewPet)target.pet);
		}
		return null;
	}

	private POOTarget find_target(POOArena arena){
		POOPet[] parr = arena.getAllPets();
		POOCoordinate coord;
		int diff;
		int min_d = 1024;
		POOPet target_pet = null;	
		POOTarget target = new POOTarget();

		for( int i = 0; i < parr.length; i++ ){
			coord = ((POONewPet)parr[i]).getCoordinate();
			diff = ((POOExCoordinate)coord).diff(this.getCoordinate());
			if( min_d > diff && parr[i] != this && parr[i].getHP() > 0 ){
				if( target_pet == null || target_pet.getHP() > parr[i].getHP() ){
					min_d = diff;
					target_pet = parr[i];
				}
			}
		}
		target.exists = false;
		target.pet = null;
		target.coord = null;
		target.distance = min_d;
		if( min_d != 1024 ){
			target.exists = true;
			target.pet = target_pet;
			target.coord = ((POONewPet)target_pet).getCoordinate();
		}
		return target;
	}

	private POOAction attack(POONewPet enermy){
		POOAction action = new POOAction();
		action.dest = enermy;
		if( enermy.getHP() <= 20 ){
			this.setMP(this.getMP()-20);
			action.skill = new POOExecute();
			action.skill.act(enermy);
		}
		else if( this.getMP() > 5 ){
			this.setMP(this.getMP()-5);
			action.skill = new POOBrutalStrike();
			action.skill.act(enermy);
		}
		else{
			action.skill = new POOBaseAttack();
			action.skill.act(enermy);
		}
		return action;
	}

	private POOCoordinate move(POONewPet enermy){
		POOCoordinate new_coord;
		POOCoordinate enermy_coord = enermy.getCoordinate();
		POOCoordinate my_coord = this.getCoordinate();
		int x, y;
		int speed = this.getAGI();
		if( Math.abs(my_coord.x-enermy_coord.x) <= speed )
			x = enermy_coord.x;
		else if( my_coord.x > enermy_coord.x )
			x = my_coord.x - speed;
		else
			x = my_coord.x + speed;
		if( Math.abs(my_coord.y-enermy_coord.y) <= speed )
			y = enermy_coord.y;
		else if( my_coord.y > enermy_coord.y )
			y = my_coord.y - speed;
		else
			y = my_coord.y + speed;
		new_coord = new POOExCoordinate(x,y);
		this.setCoordinate(new_coord);
		return new_coord;
	}
}
