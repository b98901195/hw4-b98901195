package ntu.csie.oop13spring;
import ntu.csie.oop13spring.*;

public abstract class POONewPet extends POOPet{
	private POOCoordinate coordinate;
	static protected final boolean checkCoordinate(POOCoordinate new_coord){
		boolean x = new_coord.x >= 0 && new_coord.x < 1024;
		boolean y = new_coord.y >= 0 && new_coord.y < 1024;
		return (x && y);
	}
	protected final boolean setCoordinate(POOCoordinate new_coord){
		if( checkCoordinate(new_coord) ){
			this.coordinate = new_coord;
			return true;
		}
		else{
			return false;
		}
	}
	protected final boolean setCoordinate(int x, int y){
		POOExCoordinate coord = new POOExCoordinate(x,y);
		return setCoordinate(coord);
	}
	protected final POOCoordinate getCoordinate(){
		return coordinate;
	}
}
