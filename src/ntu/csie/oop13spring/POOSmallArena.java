package ntu.csie.oop13spring;
import ntu.csie.oop13spring.*;

class POOSmallArena extends POOArena{
	private static int round = 0;
	public boolean fight(){
		POOPet[] parr = getAllPets();
		POOAction action;
		POOCoordinate move;
		int alive = 0;
		if( round == 0 )
			show();
		round++;
		System.out.printf("--------------------Round %d Start--------------------\n", round);
		for( int i = 0; i < parr.length; i++ ){
			if( parr[i].getHP() > 0 ){
				action = null;
				move = null;
				action = parr[i].act(this);
				if( action == null ){
					move = parr[i].move(this);
					if( move != null )
						this.print_move(parr[i], move);
				}
				else
					this.print_action(parr[i], action);
			}
			
		}
		for( int i = 0; i < parr.length; i++ ){
			if( parr[i].getHP() > 0 )
				alive++;
		}
		if( alive > 1 )
			return true;
		else{
			show();
			return false;
		}
	}

	public void show(){
		if( round == 0 )
			System.out.printf("--------------------Status--------------------\n");
		else
			System.out.printf("--------------------Round %d End--------------------\n", round);
		POOPet[] parr = getAllPets();
		for( int i = 0; i < parr.length; i++ ){
			this.print_pets((POONewPet)parr[i]);
		}
	}

	public POOCoordinate getPosition(POOPet p){
		POONewPet temp = (POONewPet)p;
		return temp.getCoordinate();
	}

	public void print_move(POOPet pet, POOCoordinate coordinate){
		System.out.printf("%s moves to (%d,%d)\n", pet.getName(), coordinate.x, coordinate.y);
	}

	public void print_action(POOPet pet, POOAction action){
		String name = action.skill.getClass().getName();
		name = name.substring("ntu.csie.oop13spring.".length(), name.length());
		if( name.equals("POOHeal") || name.equals("POOBloodConvert") || name.equals("POORestoreMana") || name.equals("POOChargeElectricity") )
			System.out.printf("%s use %s\n", pet.getName(), name);
		else
			System.out.printf("%s attacks %s with %s\n", pet.getName(), action.dest.getName(), name);
	}

	public void print_pets(POONewPet pet){
		System.out.printf("<%s> HP: %d, MP: %d, AGI: %d, (x,y): (%d,%d)\n", pet.getName(), pet.getHP(), pet.getMP(), pet.getAGI(), pet.getCoordinate().x, pet.getCoordinate().y);
	}
}
