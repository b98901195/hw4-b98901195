package ntu.csie.oop13spring;
import ntu.csie.oop13spring.*;

class POOExCoordinate extends POOCoordinate{
	POOExCoordinate(int x, int y){
		this.x = x;
		this.y = y;
	}
	POOExCoordinate(){
		this(0,0);
	}
	public boolean equals(POOCoordinate other){
		return ((x==other.x)&&(y==other.y));
	}

	public int diff(POOCoordinate other){
		return (Math.abs(x-other.x) + Math.abs(y-other.y));
	}
}
