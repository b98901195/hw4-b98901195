#!/bin/sh

ARENA_CLASS=ntu.csie.oop13spring.POOSmallArena
PET1_CLASS=ntu.csie.oop13spring.POOLion
PET2_CLASS=ntu.csie.oop13spring.POOPriest
PET3_CLASS=ntu.csie.oop13spring.POOVampire
PET4_CLASS=ntu.csie.oop13spring.POOPoisonSnake
PET5_CLASS=ntu.csie.oop13spring.POOPikachu

java -jar hw4.jar $ARENA_CLASS $PET1_CLASS $PET2_CLASS $PET3_CLASS $PET4_CLASS $PET5_CLASS
